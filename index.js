"use strict";
//-----------------------------------------------------------------------//
var cursos = [
    { asignatura: 'Matematicas', objetivos: 'Comprension', nota: 7.00, id: 10000000 },
    { asignatura: 'Fisica', objetivos: 'Logica', nota: 6.9, id: 10000001 }
];
var datos_alumno = {
    nombre: 'Alejando Herrera C.',
    rut: '12345678-9',
    telefono: '997654321',
    email: 'alejando.herrera.c@gmail.com',
    acudientes: 'ninguno',
    fecha_nacimiento: '2021-11-11',
    region: 'Biobio',
    comuna: 'Antuco',
    direccion: 'Calle 2, 123',
    curso: 4
};
var comunas = {
    Valparaiso: ['Isla de Pascua', 'Los Andes', 'Calle Larga', 'Rinconada', 'San Esteban',
        'Cabildo', 'La Ligua', 'Papudo', 'Petorca', 'Zapallar', 'Hijuelas',
        'La Calera', 'La Cruz', 'Nogales', 'Quillota', 'Algarrobo', 'Cartagena',
        'El Quisco', 'El Tabo', 'San Antonio', 'Santo Domingo', 'Catemu',
        'Llay-Llay', 'Panquehue', 'Putaendo', 'San Felipe', 'Santa María',
        'Casablanca', 'Concón', 'Juan Fernández', 'Puchuncaví', 'Quintero',
        'Valparaíso', 'Viña del Mar', 'Limache', 'Olmué', 'Quilpué', 'Villa Alemana'],
    Biobio: ['Arauco', 'Cañete', 'Contulmo', 'Curanilahue', 'Lebu', 'Los Álamos', 'Tirúa',
        'Alto Biobío', 'Antuco', 'Cabrero', 'Laja', 'Los Ángeles', 'Mulchén', 'Nacimiento',
        'Negrete', 'Quilaco', 'Quilleco', 'San Rosendo', 'Santa Bárbara',
        'Tucapel', 'Yumbel', 'Chiguayante', 'Concepción', 'Coronel', 'Florida',
        'Hualpén', 'Hualqui', 'Lota', 'Penco', 'San Pedro de la Paz', 'Santa Juana',
        'Talcahuano', 'Tomé'
    ]
};
//-----------------------------------------------------------------------//
function deleteCourse(id) {
    var item = document.getElementById(id);
    var table = document.getElementById('tabla-cursos');
    table.removeChild(item);
    var index = cursos.findIndex(function (curso) {
        return curso.id == id;
    });
    if (index != -1) {
        cursos.splice(index, 1);
    }
    console.log(cursos);
}
function createNewCourse() {
    var course_name = document.getElementById('anadir-asignatura');
    var objetive = document.getElementById('anadir-objetivo');
    var grade = document.getElementById('anadir-nota');
    var id = "" + (Math.floor(Math.random() * (99999999 - 10000000)) + 10000000);
    var course = document.createElement('div');
    course.classList.add('row');
    course.id = id;
    course.innerHTML = "\n        <p class='col-4'>" + course_name.value + "</p>\n        <p class='col-4'>" + objetive.value + "</p>\n        <p class='col-2'>" + grade.value + "</p>\n        <p class='col-2' onClick='deleteCourse(" + id + ")'><span class=\"material-icons-outlined link\">delete</span>\n        </p>\n    ";
    var table = document.getElementById('tabla-cursos');
    table.appendChild(course);
    cursos.push({ asignatura: course_name.value,
        objetivos: objetive.value,
        nota: grade.value,
        id: id });
}
function generateFicha() {
    var nombre = document.getElementById('ficha-nombre');
    var rut = document.getElementById('ficha-rut');
    var telefono = document.getElementById('ficha-telefono');
    var email = document.getElementById('ficha-email');
    var acudientes = document.getElementById('ficha-acudientes');
    var fecha = document.getElementById('ficha-fecha');
    var region = document.getElementById('ficha-region');
    var comuna = document.getElementById('ficha-comuna');
    var direccion = document.getElementById('ficha-direccion');
    var curso = document.getElementById('ficha-curso');
    nombre.innerHTML = "" + datos_alumno.nombre;
    rut.innerHTML = "<strong>Rut:</strong> " + datos_alumno.rut;
    telefono.innerHTML = "<strong>Telefono:</strong> " + datos_alumno.telefono;
    email.innerHTML = "<strong>Correo:</strong> " + datos_alumno.email;
    acudientes.innerHTML = "<strong>Acudientes:</strong> " + datos_alumno.acudientes;
    fecha.value = datos_alumno.fecha_nacimiento;
    region.innerHTML = "<strong>Region:</strong> " + datos_alumno.region;
    comuna.innerHTML = "<strong>Comuna:</strong> " + datos_alumno.comuna;
    direccion.innerHTML = "<strong>Direccion:</strong> " + datos_alumno.direccion;
    curso.innerHTML = "<strong>Curso: &nbsp;</strong> " + datos_alumno.curso + " primaria";
}
function generateEditar() {
    var nombre = document.getElementById('editar-nombre');
    var rut = document.getElementById('editar-rut');
    var telefono = document.getElementById('editar-telefono');
    var email = document.getElementById('editar-email');
    var acudientes = document.getElementById('editar-acudientes');
    var fecha = document.getElementById('editar-fecha');
    var region = document.getElementById('editar-region');
    var comuna = document.getElementById('editar-comuna');
    var direccion = document.getElementById('editar-direccion');
    var curso = document.getElementById('editar-curso');
    nombre.value = datos_alumno.nombre;
    rut.value = datos_alumno.rut;
    telefono.value = datos_alumno.telefono;
    email.value = datos_alumno.email;
    acudientes.value = datos_alumno.acudientes;
    fecha.value = datos_alumno.fecha_nacimiento;
    region.value = datos_alumno.region;
    direccion.value = datos_alumno.direccion;
    curso.value = datos_alumno.curso;
    updateComunas();
    comuna.value = datos_alumno.comuna;
    setValidationEditar();
}
function generateCursos() {
    var table = document.getElementById('tabla-cursos');
    for (var i = 0; i < cursos.length; i++) {
        var course = document.createElement('div');
        course.classList.add('row');
        course.setAttribute('id', cursos[i].id);
        course.innerHTML = "\n            <p class='col-4'>" + cursos[i].asignatura + "</p>\n            <p class='col-4'>" + cursos[i].objetivos + "</p>\n            <p class='col-2'>" + cursos[i].nota + "</p>\n            <p class='col-2' onClick='deleteCourse(" + cursos[i].id + ")'><span class=\"material-icons-outlined link\">delete</span>\n            </p>\n        ";
        table.appendChild(course);
    }
}
function updateComunas() {
    var region = document.getElementById('editar-region');
    var comuna = document.getElementById('editar-comuna');
    var comunas_ = comunas[region.value];
    comunas_.sort();
    comuna.innerHTML = '';
    for (var i = 0; i < comunas_.length; i++) {
        var comuna_ = document.createElement('option');
        comuna_.setAttribute('value', comunas_[i]);
        comuna_.innerHTML = comunas_[i];
        comuna.appendChild(comuna_);
    }
}
function saveFicha() {
    var nombre = document.getElementById('editar-nombre');
    var rut = document.getElementById('editar-rut');
    var telefono = document.getElementById('editar-telefono');
    var email = document.getElementById('editar-email');
    var acudientes = document.getElementById('editar-acudientes');
    var fecha = document.getElementById('editar-fecha');
    var region = document.getElementById('editar-region');
    var comuna = document.getElementById('editar-comuna');
    var direccion = document.getElementById('editar-direccion');
    var curso = document.getElementById('editar-curso');
    datos_alumno['nombre'] = nombre.value;
    datos_alumno['rut'] = rut.value;
    datos_alumno['telefono'] = telefono.value;
    datos_alumno['email'] = email.value;
    datos_alumno['acudientes'] = acudientes.value;
    datos_alumno['fecha'] = fecha.value;
    datos_alumno['region'] = region.value;
    datos_alumno['comuna'] = comuna.value;
    datos_alumno['direccion'] = direccion.value;
    datos_alumno['curso'] = curso.value;
}
function showEditar() {
    generateEditar();
    var ficha = document.getElementById('ficha');
    var editar = document.getElementById('editar');
    ficha.classList.add('hidden');
    editar.classList.remove('hidden');
}
function showFicha() {
    generateFicha();
    var ficha = document.getElementById('ficha');
    var editar = document.getElementById('editar');
    ficha.classList.remove('hidden');
    editar.classList.add('hidden');
}
function hideAddCourse() {
    var addCourse = document.getElementById('form-cursos');
    addCourse.classList.add('hidden');
}
function showAddCourse() {
    var addCourse = document.getElementById('form-cursos');
    addCourse.classList.remove('hidden');
}
//-----------------------------------------------------------------------//
function setValidationEditar() {
    'use strict';
    var form = document.getElementById('form-editar');
    form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            event.preventDefault();
            event.stopPropagation();
            saveFicha();
            showFicha();
        }
        form.classList.add('was-validated');
    }, false);
}
function setValidationCursos() {
    'use strict';
    var form = document.getElementById('form-cursos');
    form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            event.preventDefault();
            event.stopPropagation();
            createNewCourse();
            hideAddCourse();
        }
        form.classList.add('was-validated');
    }, false);
}
//-----------------------------------------------------------------------//
window.addEventListener('load', function () {
    showFicha();
    hideAddCourse();
    setValidationCursos();
    generateCursos();
});
