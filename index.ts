

//-----------------------------------------------------------------------//


var cursos:{asignatura:any,objetivos:any,nota:any,id:any}[] = [
        {asignatura: 'Matematicas', objetivos: 'Comprension', nota: 7.00, id: 10000000},
        {asignatura: 'Fisica', objetivos: 'Logica', nota: 6.9, id: 10000001}
    ]

var datos_alumno:{[key:string]:any} = {
    nombre: 'Alejando Herrera C.',
    rut: '12345678-9',
    telefono: '997654321',
    email: 'alejando.herrera.c@gmail.com',
    acudientes: 'ninguno',
    fecha_nacimiento: '2021-11-11',
    region:'Biobio',
    comuna: 'Antuco',
    direccion: 'Calle 2, 123',
    curso: 4
}

var comunas:{[key:string]:any} = {
    Valparaiso: ['Isla de Pascua','Los Andes','Calle Larga','Rinconada','San Esteban',
                'Cabildo','La Ligua','Papudo','Petorca','Zapallar','Hijuelas',
                'La Calera','La Cruz','Nogales','Quillota', 'Algarrobo','Cartagena',
                'El Quisco','El Tabo','San Antonio','Santo Domingo', 'Catemu',
                'Llay-Llay','Panquehue','Putaendo','San Felipe','Santa María',
                'Casablanca','Concón','Juan Fernández','Puchuncaví','Quintero',
                'Valparaíso','Viña del Mar', 'Limache','Olmué','Quilpué','Villa Alemana'],
    Biobio: [ 'Arauco', 'Cañete','Contulmo','Curanilahue','Lebu','Los Álamos','Tirúa',
                'Alto Biobío','Antuco','Cabrero','Laja','Los Ángeles','Mulchén','Nacimiento',
                'Negrete','Quilaco','Quilleco','San Rosendo','Santa Bárbara',
                'Tucapel','Yumbel', 'Chiguayante','Concepción','Coronel','Florida',
                'Hualpén','Hualqui','Lota','Penco','San Pedro de la Paz','Santa Juana',
                'Talcahuano','Tomé'
]
}

//-----------------------------------------------------------------------//

function deleteCourse(id:string){
    let item:any = document.getElementById(id);
    let table:any = document.getElementById('tabla-cursos')
    table.removeChild(item)

    let index:number = cursos.findIndex(curso =>{
        return curso.id == id;
    })

    if(index != -1){
        cursos.splice(index,1)
    }
    console.log(cursos)
}


function createNewCourse(){
    
    let course_name:any = document.getElementById('anadir-asignatura')
    let objetive:any = document.getElementById('anadir-objetivo')
    let grade:any = document.getElementById('anadir-nota')
    
    let id:string = `${Math.floor(Math.random()*(99999999-10000000)) + 10000000}`

    let course:any = document.createElement('div')
    course.classList.add('row')
    course.id = id
    course.innerHTML = `
        <p class='col-4'>${course_name.value}</p>
        <p class='col-4'>${objetive.value}</p>
        <p class='col-2'>${grade.value}</p>
        <p class='col-2' onClick='deleteCourse(${id})'><span class="material-icons-outlined link">delete</span>
        </p>
    `

    let table:any = document.getElementById('tabla-cursos')
    table.appendChild(course)

    cursos.push({asignatura: course_name.value,
                    objetivos:objetive.value,
                    nota:grade.value,
                    id:id})
}

function generateFicha(){
    let nombre:any = document.getElementById('ficha-nombre')
    let rut:any = document.getElementById('ficha-rut')
    let telefono:any = document.getElementById('ficha-telefono')
    let email:any = document.getElementById('ficha-email')
    let acudientes:any = document.getElementById('ficha-acudientes')
    let fecha:any = document.getElementById('ficha-fecha')
    let region:any = document.getElementById('ficha-region')
    let comuna:any = document.getElementById('ficha-comuna')
    let direccion:any = document.getElementById('ficha-direccion')
    let curso:any = document.getElementById('ficha-curso')    
    
    nombre.innerHTML = `${datos_alumno.nombre}`
    rut.innerHTML = `<strong>Rut:</strong> ${datos_alumno.rut}`
    telefono.innerHTML = `<strong>Telefono:</strong> ${datos_alumno.telefono}`
    email.innerHTML = `<strong>Correo:</strong> ${datos_alumno.email}`
    acudientes.innerHTML = `<strong>Acudientes:</strong> ${datos_alumno.acudientes}`
    fecha.value = datos_alumno.fecha_nacimiento
    region.innerHTML = `<strong>Region:</strong> ${datos_alumno.region}`
    comuna.innerHTML = `<strong>Comuna:</strong> ${datos_alumno.comuna}`
    direccion.innerHTML = `<strong>Direccion:</strong> ${datos_alumno.direccion}`
    curso.innerHTML = `<strong>Curso: &nbsp;</strong> ${datos_alumno.curso} primaria`
}

function generateEditar(){
    let nombre:any = document.getElementById('editar-nombre')
    let rut:any = document.getElementById('editar-rut')
    let telefono:any = document.getElementById('editar-telefono')
    let email:any = document.getElementById('editar-email')
    let acudientes:any = document.getElementById('editar-acudientes')
    let fecha:any = document.getElementById('editar-fecha')
    let region:any = document.getElementById('editar-region')
    let comuna:any = document.getElementById('editar-comuna')
    let direccion:any = document.getElementById('editar-direccion')
    let curso:any = document.getElementById('editar-curso')

    nombre.value = datos_alumno.nombre
    rut.value = datos_alumno.rut
    telefono.value = datos_alumno.telefono
    email.value = datos_alumno.email
    acudientes.value = datos_alumno.acudientes
    fecha.value = datos_alumno.fecha_nacimiento
    region.value = datos_alumno.region
    direccion.value = datos_alumno.direccion
    curso.value = datos_alumno.curso

    updateComunas()
    comuna.value = datos_alumno.comuna
    
    setValidationEditar()
}

function generateCursos(){
    
    let table:any = document.getElementById('tabla-cursos')

    for(let i=0 ; i<cursos.length ; i++){
        let course:any = document.createElement('div')
        course.classList.add('row')
        course.setAttribute('id', cursos[i].id)
        course.innerHTML = `
            <p class='col-4'>${cursos[i].asignatura}</p>
            <p class='col-4'>${cursos[i].objetivos}</p>
            <p class='col-2'>${cursos[i].nota}</p>
            <p class='col-2' onClick='deleteCourse(${cursos[i].id})'><span class="material-icons-outlined link">delete</span>
            </p>
        `
        table.appendChild(course)
    }
}

function updateComunas(){
    let region:any = document.getElementById('editar-region')
    let comuna:any = document.getElementById('editar-comuna')

    let comunas_:any = comunas[region.value]
    comunas_.sort()

    comuna.innerHTML = ''
    for(let i:number=0 ; i<comunas_.length ; i++){
        let comuna_:any = document.createElement('option')
        comuna_.setAttribute('value', comunas_[i])
        comuna_.innerHTML = comunas_[i]
        comuna.appendChild(comuna_)
    }
}

function saveFicha(){
    let nombre:any = document.getElementById('editar-nombre')
    let rut:any = document.getElementById('editar-rut')
    let telefono:any = document.getElementById('editar-telefono')
    let email:any = document.getElementById('editar-email')
    let acudientes:any = document.getElementById('editar-acudientes')
    let fecha:any = document.getElementById('editar-fecha')
    let region:any = document.getElementById('editar-region')
    let comuna:any = document.getElementById('editar-comuna')
    let direccion:any = document.getElementById('editar-direccion')
    let curso:any = document.getElementById('editar-curso')

    datos_alumno['nombre'] = nombre.value
    datos_alumno['rut'] = rut.value
    datos_alumno['telefono'] = telefono.value
    datos_alumno['email'] = email.value
    datos_alumno['acudientes'] = acudientes.value
    datos_alumno['fecha'] = fecha.value
    datos_alumno['region'] = region.value
    datos_alumno['comuna'] = comuna.value
    datos_alumno['direccion'] = direccion.value
    datos_alumno['curso'] = curso.value

}

function showEditar(){
    generateEditar()
    let ficha:any = document.getElementById('ficha')
    let editar:any = document.getElementById('editar')
    
    ficha.classList.add('hidden')
    editar.classList.remove('hidden')
}

function showFicha(){
    generateFicha()
    let ficha:any = document.getElementById('ficha')
    let editar:any = document.getElementById('editar')

    ficha.classList.remove('hidden')
    editar.classList.add('hidden')
}

function hideAddCourse(){
    let addCourse:any = document.getElementById('form-cursos')
    addCourse.classList.add('hidden')
}

function showAddCourse(){
    let addCourse:any = document.getElementById('form-cursos')
    addCourse.classList.remove('hidden')
}

//-----------------------------------------------------------------------//
function setValidationEditar() {
    'use strict'
    var form:any = document.getElementById('form-editar')
  
    
    form.addEventListener('submit', function (event:any) {
        if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
        }else{
            event.preventDefault()
            event.stopPropagation()
            saveFicha()
            showFicha()
        }
        form.classList.add('was-validated')
        }, false)
      
  }

  function setValidationCursos() {
    'use strict'
    var form:any = document.getElementById('form-cursos')
  
    
    form.addEventListener('submit', function (event:any) {
        if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
        }else{
            event.preventDefault()
            event.stopPropagation()
            createNewCourse()
            hideAddCourse()
        }
        form.classList.add('was-validated')
        }, false)
      
  }

//-----------------------------------------------------------------------//

window.addEventListener('load', ()=>{
    showFicha()
    hideAddCourse()
    setValidationCursos()
    generateCursos()
})